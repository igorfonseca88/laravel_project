@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Movies</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group" id="divSearch" style="display:block;">
                        <label for="search">Search a movie by name:</label>
                        <div class="d-flex">
                            <input type="text" class="form-control" id="search"  placeholder="Type here the name">
                            <button type="button" onclick="getMoviesByName()" class="btn btn-primary">Search</button>
                        </div>
                    </div>

                    <div class="list-group" id="genres" style="display:block;">
                        <p>Or select a genre: </p>
                    </div>
                    <div class="list-group" id="movies" style="display:none;">
                        <p>Select a movie to see more details: </p>
                    </div>

                    <div id="divDetails" style="display:none;">
                        <p id="title"></p>
                        <p id="overview"></p>
                        <p id="released"></p>
                        <p id="rating"></p>
                        <p id="image"></p>
                    </div>
                    
                </div>
                <div class="card-footer">
                    <a href="/home" class="btn btn-primary">New search</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
var token = '{{ env('API_KEY') }}';
var url = "https://api.themoviedb.org/3/";

function getApiGenre(){
   var settings = {
       "async": true,
       "crossDomain": true,
       "url": url+ "genre/movie/list?language=en-US&api_key="+token,
       "method": "GET"
   }

   $.getJSON(settings).done(function (data) {
       var arrayGenres = data.genres;
       option = '';
       for(i=0; i< arrayGenres.length; i++){
           option += '<button type="button" class="list-group-item list-group-item-action" onclick="getMoviesByGenre(' + arrayGenres[i].id +')"> ' + arrayGenres[i].name +'</button>';
       }
       $('#genres').append(option);
   })
   .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    });
}

function getMoviesByGenre(idGenre){
   
   var settings = {
       "async": true,
       "crossDomain": true,
       "url": url+ "discover/movie?with_genres="+ idGenre +"&sort_by=popularity.desc&api_key="+token,
       "method": "GET"
   }

   $('#movies').append("");
   $.getJSON(settings).done(function (data) {
       var arrayMovies = data.results;
       optionMovies = '';
       for(i=0; i< arrayMovies.length; i++){
           optionMovies += '<button type="button" class="list-group-item list-group-item-action" onclick="getMovieById(' + arrayMovies[i].id +')"> ' + arrayMovies[i].title +'</button>';

       }
       
       $('#movies').append(optionMovies);
       $('#movies').show();
       $('#genres').hide();
       $('#divSearch').hide();
    })
    .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    });
}

function getMovieById(idMovie){

   var settings = {
       "async": true,
       "crossDomain": true,
       "url": url+ "movie/"+idMovie+"?language=en-US&api_key="+token,
       "method": "GET"
   }

   $.getJSON(settings).done(function (data) {
       $('#title').append("Title: "+data.original_title);
       $('#overview').append("Overview: "+data.overview);
       $('#released').append("Released: "+formatDate(data.release_date));
       $('#rating').append("Rating: "+data.vote_average);
       $('#budget').append("Budget: "+data.budget);

       var url = 'https://image.tmdb.org/t/p/w500' + data.backdrop_path;
       $('#image').append("<img src='"+url+"'>")

       $('#divDetails').show();
       $('#movies').hide();
       $('#genres').hide();
       $('#divSearch').hide();
   })
   .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    });
}

function getMoviesByName(){
var searchValue = $('#search').val();
var settings = {
    "async": true,
    "crossDomain": true,
    "url": url+ "search/movie?query="+searchValue+"&sort_by=popularity.desc&api_key="+token,
    "method": "GET"
}

$('#movies').append('');
    $.getJSON(settings).done(function (data) {
        var arrayMovies = data.results;
        optionMovies = '';
        if(arrayMovies.length > 0){
            for(i=0; i< arrayMovies.length; i++){
                optionMovies += '<button type="button" class="list-group-item list-group-item-action" onclick="getMovieById(' + arrayMovies[i].id +')"> ' + arrayMovies[i].title +'</button>';

            }
        }else{
            optionMovies = '<p>No movie was found. Try again!</p>';
        }
        
        $('#movies').append(optionMovies);
        $('#movies').show();
        $('#genres').hide();
        $('#divSearch').hide();
    })
    .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
    });
}

function formatDate(date){
    var current_datetime = new Date()
    var formatted_date = current_datetime.getDate() + "/" + (current_datetime.getMonth() + 1) + "/" + current_datetime.getFullYear()
    return formatted_date;
}

$(function(){
   getApiGenre();      
});
</script>
@endsection
