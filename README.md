# webmovies

Web app that return The Genres and Movies using the API from
https://www.themoviedb.org/documentation/api.

**Technologies:**

PHP: >=7.1.3

Framework Laravel: 5.6

Composer - Dependency Management for PHP


**Steps**

1 - run the command composer install 

2 - configure your database credentials in .env > DB_DATABASE / DB_USERNAME / DB_PASSWORD

3 - execute the migrate scripts, php artisan make:migrate

4 - npm install

5 - npm run dev

6 - inform your access key to use the api in .env > API_KEY

7 - start the serve, php artisan serve


